pragma solidity ^0.4.24;

//Limiting permission to the owner of the contract 
// contract Owner {
//     address owner;

//     constructor () public {
//         owner = msg.sender;
//     }

//     //setting modifier to limit calls
//     modifier onlyOwner {
//         require(owner == msg.sender);
//         _;
//     }
// }




contract Election {
    // Model Candidate
    // struct Candidate {
    //     uint id;
    //     string name;
    //     uint voteCount;
    // }

    // // Store Candidate
    // // Fetch Candidates
    // mapping(uint => Candidate) public candidates;

    // // Store Candidates count
    // uint public candidatesCount;

    // function Election () public {
    //     addCandidate("Candidate 1");
    //     addCandidate("Candidate 2");
    // }

    // function addCandidate (string _name) private {
    //     candidatesCount ++;
    //     candidates[candidatesCount] = Candidate(candidatesCount, _name, 0);
    // }

    // struct User{
    //     bytes16 fName;
    //     bytes16 lName;
    //     uint id;
    // }

    address recipient;
    uint amt_sent;    

    //store multiple users' details
    // mapping(address => User) public users;
    // address[] userAccounts;

    //fetch all user accounts
    // function addUser(address _address, bytes16 _fname, bytes16 _lname, uint _id) public onlyOwner {
    //     var newUser = users[_address];
        
    //     newUser.fName = _fname;
    //     newUser.lName = _lname;
    //     newUser.id = _id;

    //     userAccounts.push(_address) -1;
    // }

    //Query a users by address
    // function getUser() view public returns (address[]){
    //     return userAccounts;
    // }

    //Query a specific user by address
    // function getUser(address _address) view public returns (bytes16, bytes16, uint){
    //     return (users[_address].fName, users[_address].lName, users[_address].id);
    // }

    // function getUsersCount() view public returns (uint){
    //     return userAccounts.length;
    // }

    event receiverMsg(string msg_successful);
    
    //transferring ether from a contract to another account
    function withdraw() public {
        msg.sender.transfer(address(this).balance);
        emit receiverMsg("Sent successfully");
    }

    //Added the receiving account as a parameter
    function deposit(uint256 amount) payable public {
        require(msg.value == amount);
    }

    function getBalance() public view returns (uint256) {
        return address(this).balance;
    }





}